class Consts {
  Consts._();

  static const int colorPrimary = 0xFFff8900;
  static const int colorPrimaryTrans = 0x80ff8900;
  static const int colorText = 0xFF2e3a45;
  static const int colorGrey = 0xFF757575;
  static const int colorLightGrey = 0xFFF6F7F9;
  static const int colorBGLightGrey = 0xFFFEFEFE;
  static const int colorBlackAlpha = 0x312e3a45;
  static const int colorOrangeAlpha = 0x31ff8900;
  static const int colorOrangeAlphaLight = 0x12ff8900;
  static const int colorBlueAlpha = 0x31008cff;
  static const int colorGreenAlpha = 0x3100ff0f;
  static const int colorRedishOrange = 0xFFF63A05;
  static const int colorYellow = 0xFFF6B800;

  static const double padding = 16.0;
  static const double avatarRadius = 50.0;

  static const String HOME = 'Home';
  static const String ABOUT = 'About';

  static const String LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

  static const String PRODUCT_SANS = 'Product Sans';

  static const String IMAGES_DIRECTORY = 'assets/images/';
  static const String LOGO = IMAGES_DIRECTORY + 'logo.png';
  static const String IC_SEARCH = IMAGES_DIRECTORY + 'ic_search.png';
  static const String IC_STAR = IMAGES_DIRECTORY + 'star.png';
  static const String IC_PERSON = IMAGES_DIRECTORY + 'placeholder_person.png';

  static const String PLACEHOLDER_CAR = IMAGES_DIRECTORY + 'placeholder_car.png';
  static const String PLACEHOLDER_LOADING_CAR = IMAGES_DIRECTORY + 'car_image_loading.png';
  static const String USER = IMAGES_DIRECTORY + 'user.png';
  static const String AC = IMAGES_DIRECTORY + 'fan.png';
  static const String MULTIMEDIA = IMAGES_DIRECTORY + 'video.png';

  static const String TYPE_PREMIUM = 'premium';
  static const String TYPE_STANDARD = 'standard';
  static const String TYPE_CLASSIC = 'classic';

  static const String KEY_AMENITIES = 'amenities';
  static const String KEY_CAPACITY = 'capacity';
  static const String KEY_IS_AC = 'isAc';
  static const String KEY_IS_MULTIMEDIA = 'isMultimedia';
  static const String KEY_AVAILABILITY = 'availability';
  static const String KEY_CITY = 'city';
  static const String KEY_COLOR = 'color';
  static const String KEY_DETAILS = 'details';
  static const String KEY_FARE = 'fare';
  static const String KEY_IMAGE_URL = 'imgUrl';
  static const String KEY_IMAGES = 'images';
  static const String KEY_NAME = 'name';
  static const String KEY_PRICE_UNIT = 'priceUnit';
  static const String KEY_RATING = 'rating';
  static const String KEY_TYPE = 'type';
  static const String KEY_YEAR = 'year';
  static const String KEY_DRIVER = 'driver';
  static const String KEY_CONTACT_NO = 'contactNo';
  static const String KEY_QUERY = 'query';

  static const String FB_CHANGE_ADDED = 'added';
  static const String FB_CHANGE_MODIFIED = 'modified';
  static const String FB_CHANGE_REMOVED = 'removed';

  static const String DB_PATH_STORAGE = 'gs://yamwheels.appspot.com/';
  static const String DB_PATH_VEHICLES = 'vehicles';
  static const String DB_PATH_DRIVERS = 'drivers';
  static const String DB_PATH_QUERIES = 'queries';
}
