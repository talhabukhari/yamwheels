import 'package:firebase/firebase.dart';
import 'package:flutter/material.dart';

import 'Consts.dart';
import 'views/home/home_view.dart';

//void main() => runApp(MyApp());


//rules_version = '2';
//service cloud.firestore {
//match /databases/{database}/documents {
//
//// This rule allows anyone on the internet to view, edit, and delete
//// all data in your Firestore database. It is useful for getting
//// started, but it is configured to expire after 30 days because it
//// leaves your app open to attackers. At that time, all client
//// requests to your Firestore database will be denied.
////
//// Make sure to write security rules for your app before that time, or else
//// your app will lose access to your Firestore database
//match /{document=**} {
//allow read, write: if request.time < timestamp.date(2020, 3, 3);
//}
//}
//}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initializeApp(
      apiKey: "AIzaSyAile91L9Fuu3LuTw9d9NMGz_wMiz1usXg",
      authDomain: "yamwheels.firebaseapp.com",
      databaseURL: "https://yamwheels.firebaseio.com",
      projectId: "yamwheels",
      storageBucket: "yamwheels.appspot.com",
//      messagingSenderId: "25213536874",
//      appId: "1:25213536874:web:fe5896e87fc2cc42cf9813"
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Color(Consts.colorPrimary),
        accentColor: Color(Consts.colorPrimary),
        cursorColor: Color(Consts.colorPrimary),
        textTheme:
            Theme.of(context).textTheme.apply(fontFamily: Consts.PRODUCT_SANS),
      ),
      debugShowCheckedModeBanner: false,
      home: HomeView(),
//      home: SplashScreen(),
    );
  }
}
