import 'package:flutter/material.dart';

import 'navbar_logo.dart';

class NavigationBarMobile extends StatelessWidget {

  GlobalKey<ScaffoldState> homeScaffoldKey;
  NavigationBarMobile({Key key, @required this.homeScaffoldKey}) : super(key: key);

//  const NavigationBarMobile({Key key}) : super(key: key);

  openMyDrawer() {
    print('----open-----');
    homeScaffoldKey.currentState.openDrawer();
//    if (_scaffoldKey.currentState.isDrawerOpen) {
//      _scaffoldKey.currentState.showSnackBar(SnackBar(
//        content: Text("Drawer is Open"),
//      ));
//    } else {
//      _scaffoldKey.currentState.showSnackBar(SnackBar(
//        content: Text("Drawer is Closed"),
//      ));
//    }
  }

  @override
  Widget build(BuildContext context) {
  GlobalKey<ScaffoldState> _homeScaffoldKey = new GlobalKey<ScaffoldState>();
  double statusBarHeight = MediaQuery.of(context).padding.top;
    return Container(
      height: 60,
      margin: EdgeInsets.only(top: statusBarHeight),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {openMyDrawer();},
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child:  NavBarLogo(),
          ),
        ],
      ),
    );
  }
}