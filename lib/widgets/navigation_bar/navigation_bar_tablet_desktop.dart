import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Consts.dart';
import 'navbar_item.dart';
import 'navbar_logo.dart';

class NavigationBarTabletDesktop extends StatelessWidget {
  const NavigationBarTabletDesktop({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
//      CenteredView(
//      child:
      Container(
        height: 100,
        margin: EdgeInsets.only(left: 16.0, right: 16.0),
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              NavBarLogo(),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  NavBarItem(Consts.HOME),
                  SizedBox(
                    width: 20,
                  ),
                  NavBarItem(Consts.ABOUT),
                ],
              )
            ],
//        ),
          ),
        ),
    );
  }
}
