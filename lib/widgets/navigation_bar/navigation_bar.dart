import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'navigation_bar_mobile.dart';
import 'navigation_bar_tablet_desktop.dart';

class NavigationBar extends StatelessWidget {

    GlobalKey<ScaffoldState> homeScaffoldKey;
    NavigationBar({Key key, @required this.homeScaffoldKey}) : super(key: key);

//  const NavigationBar({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: NavigationBarMobile(homeScaffoldKey: homeScaffoldKey),
      tablet: NavigationBarTabletDesktop(),
    );
  }
}

