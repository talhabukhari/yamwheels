import 'package:flutter/material.dart';

import '../../Consts.dart';

class NavBarLogo extends StatelessWidget {
  const NavBarLogo({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
//      height: 120,
//      width: 120,
      child: Image.asset(Consts.LOGO),
    );
  }
}