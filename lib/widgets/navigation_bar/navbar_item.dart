import 'package:flutter/material.dart';
import 'package:yamwheels/views/home/home_view.dart';

import '../../Consts.dart';

class NavBarItem extends StatelessWidget {
  final String title;

  const NavBarItem(this.title);

  performClick(BuildContext context) {
    if (title == Consts.HOME) {
      Navigator.pop(context);
      Navigator.pushReplacement(
        context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => HomeView(),
          transitionsBuilder: (c, anim, a2, child) =>
              FadeTransition(opacity: anim, child: child),
          transitionDuration: Duration(milliseconds: 300),
        ),
      );
    } else if (title == Consts.ABOUT) {

    }
  }

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () => {performClick(context)},
      child: Text(
        title,
        style: TextStyle(
            fontSize: 18,
            fontFamily: Consts.PRODUCT_SANS,
            fontWeight: FontWeight.w400),
      ),
    );
//    return Text(
//      title,
//      style: TextStyle(
//          fontSize: 18,
//          fontFamily: Consts.PRODUCT_SANS,
//          fontWeight: FontWeight.w400),
//    );
  }
}
