import 'package:flutter/material.dart';
import 'package:yamwheels/views/home/home_view.dart';

import '../../Consts.dart';
import 'drawer_item.dart';
import 'navigation_drawer_header.dart';

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 16)]),
      child: Column(
        children: <Widget>[
          NavigationDrawerHeader(),
          GestureDetector(
            onTap: () => {
              Navigator.pop(context),
              Navigator.pushReplacement(
                context,
                PageRouteBuilder(
                  pageBuilder: (c, a1, a2) => HomeView(),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 300),
                ),
              )
            },
            child: DrawerItem(Consts.HOME, Icons.home),
          ),
          GestureDetector(
            onTap: () => {

            },
            child: DrawerItem(Consts.ABOUT, Icons.help),
          ),
        ],
      ),
    );
  }
}
