import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Consts.dart';
import 'widgets/EnsureVisibleWhenFocused.dart';
import 'models/VehicleInfo.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  FocusNode _focusNodeSearch = new FocusNode();
  final TextEditingController _searchController = new TextEditingController();

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  var _premiumDataList = <VehicleInfo>[];
  var _standardDataList = <VehicleInfo>[];
  var _classicDataList = <VehicleInfo>[];

  doSearch(String query) async {}

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

//    _premiumDataList.add(new VehicleInfo(Consts.BMWX1, 'BMW X1', '2017', 'White', 'On Evenings', '2500', 4.8, 'Lahore'));
//    _premiumDataList.add(new VehicleInfo(Consts.AUDI, 'Audi A6 Business Class', '2016', 'Black', 'On Weekends', '2000', 4.8, 'Lahore'));
//    _premiumDataList.add(new VehicleInfo(Consts.MERCEDEEZ, 'Mercedeez Benz S Class', '2013', 'White', 'On Weekends', '2000', 4.6, 'Lahore'));
//    _premiumDataList.add(new VehicleInfo(Consts.TOYOTA, 'Toyota V8', '2015', 'White', 'On Evenings', '1900', 4.6, 'Lahore'));
//    _standardDataList.add(new VehicleInfo(Consts.CIVIC, 'Honda Civic Oriel', '2017', 'Black', 'On Weekends', '900', 4.7, 'Lahore'));
//    _standardDataList.add(new VehicleInfo(Consts.ALTIS, 'Toyota Corolla Altis', '2018', 'White', 'At Morning', '800', 4.6, 'Lahore'));
//    _standardDataList.add(new VehicleInfo(Consts.ASPIRE, 'Honda City Aspire', '2016', 'Silver', 'On Evenings', '650', 4.6, 'Lahore'));
//    _standardDataList.add(new VehicleInfo(Consts.GLI, 'Toyota Corolla GLI', '2014', 'Blue', '24 Hours', '500', 4.2, 'Lahore'));
//    _classicDataList.add(new VehicleInfo(Consts.ALTO, 'Suzuki Alto', '2019', 'White', 'Anytime', '450', 4.8, 'Lahore'));
//    _classicDataList.add(new VehicleInfo(Consts.CULTUS, 'Suzuki Cultus VXL', '2019', 'Blue', 'On Weekends', '350', 4.6, 'Lahore'));
//    _classicDataList.add(new VehicleInfo(Consts.MEHRAN, 'Suzuki Mehran VXR', '2014', 'Red', 'Anytime', '200', 4.5, 'Lahore'));
//    _classicDataList.add(new VehicleInfo(Consts.FX, 'Suzuki FX', '1997', 'Black', 'Anytime', '300', 4.5, 'Lahore'));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double statusBarHeight = MediaQuery.of(context).padding.top;

    /*24 is for notification bar on Android*/
//    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemHeight = 320;
    final double itemWidth = size.width / 2;

    final backgroundContainer = Center(
        child: Container(
      width: size.width,
      height: size.height,
      color: Color(Consts.colorBGLightGrey),
    ));

    final searchEditTextField = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 48.0,
          child: EnsureVisibleWhenFocused(
            focusNode: _focusNodeSearch,
            child: Padding(
              padding: EdgeInsets.only(left: 16.0, right: 4.0),
              child: Container(
                child: TextFormField(
                  style: TextStyle(
                    fontFamily: Consts.PRODUCT_SANS,
                    fontWeight: FontWeight.w400,
                    fontSize: 16.0,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(4.0),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.transparent,
                      ),
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    hintText: 'Search...',
                    hintStyle: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0,
                      color: Colors.blueGrey[300],
                    ),
                  ),
                  keyboardType: TextInputType.text,
                  maxLines: 1,
                  controller: _searchController,
                  focusNode: _focusNodeSearch,
                ),
              ),
            ),
          ),
        ),
      ],
    );
    final searchBarContainer = Container(
      alignment: Alignment.topRight,
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Container(
          height: 48.0,
          width: 400.0,
          decoration: BoxDecoration(
            color: Color(Consts.colorLightGrey),
            borderRadius: BorderRadius.all(
              Radius.circular(24.0),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: searchEditTextField,
              ),
              GestureDetector(
                onTap: () => {
                  FocusScope.of(context).requestFocus(new FocusNode()),
                  doSearch(_searchController.text)
                },
                child: Padding(
                    padding: EdgeInsets.all(12.0),
                    child: Image.asset(
                      Consts.IC_SEARCH,
                      height: 24,
                      width: 24.0,
                      color: Color(Consts.colorPrimary),
                    )),
              ),
            ],
          ),
        ),
      ),
    );

    final logoContainer = Container(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Container(
          height: 48.0,
          width: 160,
          alignment: Alignment.center,
          child: Text(
            'YAMWheels',
            style: TextStyle(
              fontFamily: Consts.PRODUCT_SANS,
              fontWeight: FontWeight.w800,
              fontSize: 28.0,
              color: Color(Consts.colorPrimary),
            ),
          ),
        ),
      ),
    );

    final topContainer = Stack (
      children: <Widget>[
        logoContainer,
        searchBarContainer,
      ],
    );

    final premiumTopView = Container(
      margin: EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Padding(
                  padding: EdgeInsets.only(left: 16.0),
                  child: Text(
                    'Premium',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 18.0,
                      color: Color(Consts.colorText),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => {},
                child: Padding(
                  padding: EdgeInsets.only(right: 24.0),
                  child: Text(
                    'See All',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 0.0,
          ),
        ],
      ),
    );
    final premiumGridViewContainer = Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, right: 0.0, left: 0.0),
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            key: _formKey,
            child: GridView.builder(
              shrinkWrap: true,
              itemCount: _premiumDataList.length,
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 8),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                childAspectRatio: (itemWidth / itemHeight),
              ),
              itemBuilder: (context, index) {
                return _buildCard(_premiumDataList[index]);
              },
            ),
          ),
        ),
      ),
    );

    final standardTopView= Container(
      margin: EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Padding(
                  padding: EdgeInsets.only(left: 16.0),
                  child: Text(
                    'Standard',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 18.0,
                      color: Color(Consts.colorText),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => {},
                child: Padding(
                  padding: EdgeInsets.only(right: 24.0),
                  child: Text(
                    'See All',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 0.0,
          ),
        ],
      ),
    );
    final standardGridViewContainer = Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, right: 0.0, left: 0.0),
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            child: GridView.builder(
              shrinkWrap: true,
              itemCount: _standardDataList.length,
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 8),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                childAspectRatio: (itemWidth / itemHeight),
              ),
              itemBuilder: (context, index) {
                return _buildCard(_standardDataList[index]);
              },
            ),
          ),
        ),
      ),
    );

    final classicTopView = Container(
      margin: EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Padding(
                  padding: EdgeInsets.only(left: 16.0),
                  child: Text(
                    'Classic',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 18.0,
                      color: Color(Consts.colorText),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => {},
                child: Padding(
                  padding: EdgeInsets.only(right: 24.0),
                  child: Text(
                    'See All',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 0.0,
          ),
        ],
      ),
    );
    final classicGridViewContainer = Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, right: 0.0, left: 0.0),
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            child: GridView.builder(
              shrinkWrap: true,
              itemCount: _classicDataList.length,
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 8),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                childAspectRatio: (itemWidth / itemHeight),
              ),
              itemBuilder: (context, index) {
                return _buildCard(_classicDataList[index]);
              },
            ),
          ),
        ),
      ),
    );

    final itemCard = GestureDetector(
      onTap: () => {},
      child: Card(
        elevation: 4.0,
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0)),
        margin:
        EdgeInsets.only(top: 0.0, bottom: 16.0, right: 16.0, left: 16.0),
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Container(
            width: size.width / 5,
            margin:
            EdgeInsets.only(top: 0.0, right: 0.0, left: 0.0, bottom: 0.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(6.0),
                          child: Image.asset(
                            Consts.PLACEHOLDER_CAR,
                            width: 110.0,
                            height: 110.0,
                            fit: BoxFit.cover,
                          ),
                        ),
                        SizedBox(
                          width: 16.0,
                        ),
                        Flexible(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Flexible(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'BMW X1',
                                          style: TextStyle(
                                              fontFamily: 'Product Sans',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 18.0,
                                              color: Colors.black),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        '4.8',
                                        style: TextStyle(
                                            fontFamily: 'Product Sans',
                                            fontWeight: FontWeight.w400,
                                            color: Color(Consts.colorYellow),
                                            fontSize: 14.0),
                                      ),
                                      SizedBox(
                                        width: 4.0,
                                      ),
                                      Image.asset(Consts.IC_STAR,
                                          width: 14.0,
                                          height: 14.0,
                                          color: Color(Consts.colorYellow)),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 4.0,
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    '2017',
                                    style: TextStyle(
                                        fontFamily: 'Product Sans',
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16.0,
                                        color: Color(Consts.colorGrey)),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Text(
                                    ' | ',
                                    style: TextStyle(
                                        fontFamily: 'Product Sans',
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16.0,
                                        color: Color(Consts.colorGrey)),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Text(
                                    'White',
                                    style: TextStyle(
                                        fontFamily: 'Product Sans',
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16.0,
                                        color: Color(Consts.colorGrey)),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 16.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Flexible(
                                    child: Text(
                                      '',
                                      style: TextStyle(
                                          fontFamily: 'Product Sans',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 18.0,
                                          color: Colors.black),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  Text(
                                    'Starts at',
                                    style: TextStyle(
                                        fontFamily: 'Product Sans',
                                        fontWeight: FontWeight.w400,
                                        color: Color(Consts.colorGrey),
                                        fontSize: 14.0),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 4.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Flexible(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'On Weekends',
                                          style: TextStyle(
                                              fontFamily: 'Product Sans',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 16.0,
                                              color: Color(Consts.colorGrey)),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Rs 2500',
                                        style: TextStyle(
                                            fontFamily: 'Product Sans',
                                            fontWeight: FontWeight.w800,
                                            color: Color(Consts.colorPrimary),
                                            fontSize: 16.0),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
    final premiumListContainer = Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, right: 0.0, left: 0.0),
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            key: _formKey,
            child: ListView.builder(
              itemCount: _premiumDataList.length,
              itemBuilder: (context, position) {
                return _buildCard(_premiumDataList[position]);
              },
            ),
          ),
        ),
      ),
    );
    final premiumGridContainer = Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, right: 0.0, left: 0.0),
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new GridView.count(
            crossAxisCount: 4,
            childAspectRatio: 1.0,
            padding: const EdgeInsets.all(4.0),
            mainAxisSpacing: 4.0,
            crossAxisSpacing: 4.0,

            controller: new ScrollController(keepScrollOffset: false),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,

            children: new List<Widget>.generate(_premiumDataList.length, (index) {
              return new GridTile(
                child: _buildCard(_premiumDataList[index]),
              );
            }),
          ),
        ),
      ),
    );

    final bodyContent = Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.only(top: statusBarHeight),
      child: Padding(
        padding: EdgeInsets.all(0.0),
        child: Column(
          children: <Widget>[
//            searchBarContainer,
            topContainer,
            premiumTopView,
            premiumGridViewContainer,
            standardTopView,
            standardGridViewContainer,
            classicTopView,
            classicGridViewContainer,
          ],
        ),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          backgroundContainer,
          bodyContent,
        ],
      ),
    );
  }

  Widget _buildCard(VehicleInfo object) {
    String imgUrl = '';
    String name = object.name;
    String modelYear = object.year;
    String color = object.color;
    String availability = object.availability;
    String price = 'Rs ${object.fare}';
    var rating = object.rating;

    final row = GestureDetector(
        onTap: () => {},
        child: Container(
          child: Card(
            semanticContainer: true,
            elevation: 1.0,
            color: Colors.white,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8.0)),
            margin: EdgeInsets.only(top: 4.0, bottom: 8.0, right: 16.0, left: 16.0),
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Container(
                margin:
                EdgeInsets.only(top: 0.0, right: 0.0, left: 0.0, bottom: 0.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(6.0),
//                              child: CachedNetworkImage(
//                                width: 110.0,
//                                height: 110.0,
//                                imageUrl: imgUrl,
//                                fit: BoxFit.cover,
//                                placeholder: (context, url) =>
//                                    Image.asset(Consts.IC_CAR,),
//                              )
                              child: Image.asset(
                                imgUrl,
                                width: 110.0,
                                height: 110.0,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(
                              width: 16.0,
                            ),
                            Flexible(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Flexible(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              name,
                                              style: TextStyle(
                                                  fontFamily: 'Product Sans',
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 16.0,
                                                  color: Colors.black),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: 4.0,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            rating.toString(),
                                            style: TextStyle(
                                                fontFamily: 'Product Sans',
                                                fontWeight: FontWeight.w400,
                                                color: Color(Consts.colorText),
                                                fontSize: 14.0),
                                          ),
                                          SizedBox(
                                            width: 4.0,
                                          ),
                                          Image.asset(Consts.IC_STAR,
                                              width: 14.0,
                                              height: 14.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 4.0,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        modelYear,
                                        style: TextStyle(
                                            fontFamily: 'Product Sans',
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16.0,
                                            color: Color(Consts.colorGrey)),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      Text(
                                        ' | ',
                                        style: TextStyle(
                                            fontFamily: 'Product Sans',
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16.0,
                                            color: Color(Consts.colorGrey)),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      Text(
                                        color,
                                        style: TextStyle(
                                            fontFamily: 'Product Sans',
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16.0,
                                            color: Color(Consts.colorGrey)),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 16.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Flexible(
                                        child: Text(
                                          '',
                                          style: TextStyle(
                                              fontFamily: 'Product Sans',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 18.0,
                                              color: Colors.black),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      Text(
                                        'Starts at',
                                        style: TextStyle(
                                            fontFamily: 'Product Sans',
                                            fontWeight: FontWeight.w400,
                                            color: Color(Consts.colorGrey),
                                            fontSize: 14.0),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 4.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Flexible(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              availability,
                                              style: TextStyle(
                                                  fontFamily: 'Product Sans',
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 16.0,
                                                  color: Color(Consts.colorGrey)),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            price,
                                            style: TextStyle(
                                                fontFamily: 'Product Sans',
                                                fontWeight: FontWeight.w800,
                                                color: Color(Consts.colorPrimary),
                                                fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ]),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
    );
    return row;
  }
}
