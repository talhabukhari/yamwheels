import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';

import 'Consts.dart';
import 'HomeScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    (Platform.isAndroid)
        ? Timer(Duration(seconds: 3), () => _goToHomeScreen())
        : _goToHomeScreen();
  }

  _goToHomeScreen() async {
    Navigator.pushReplacement(
      context,
      PageRouteBuilder(
        pageBuilder: (c, a1, a2) => HomeScreen(),
        transitionsBuilder: (c, anim, a2, child) =>
            FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 300),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Size size = MediaQuery.of(context).size;

//    final backgroundContainer = Container(
//      width: size.width,
//      height: size.height,
//      child: Image.asset(
//        'images/login_bg.png',
//        fit: BoxFit.cover,
//      ),
//    );

    return Scaffold(
      body: Stack(
        children: <Widget>[
//          Center(
//            child: backgroundContainer,
//          ),
//          Container(
//            alignment: Alignment.bottomCenter,
//            margin: EdgeInsets.only(bottom: 80.0),
//            child: Padding(
//              padding: EdgeInsets.all(80.0),
//              child: Image.asset(
//                'images/logo.png',
//              ),
//            ),
//          ),
          Container(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                'POWERED BY YAMSOL LLC',
                style: TextStyle(
                    fontFamily: Consts.PRODUCT_SANS, fontWeight: FontWeight.w400),
              ),
            ),
          )
        ],
      ),
    );
  }
}
