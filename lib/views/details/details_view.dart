import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:yamwheels/models/VehicleInfo.dart';
import 'package:yamwheels/widgets/centered_view/centered_view.dart';
import 'package:yamwheels/widgets/nav_drawer/navigation_drawer.dart';
import 'package:yamwheels/widgets/navigation_bar/navigation_bar.dart';

import 'details_content_desktop.dart';
import 'details_content_mobile.dart';


class DetailsView extends StatelessWidget {

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  VehicleInfo vehicleInfo;

  DetailsView({Key key, @required this.vehicleInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) =>
          Scaffold(
            key: _scaffoldKey,
            drawer: sizingInformation.deviceScreenType ==
                DeviceScreenType.Mobile
                ? NavigationDrawer()
                : null,
            backgroundColor: Colors.white,
            body: Container(
              child: Column(
                children: <Widget>[
                  NavigationBar(homeScaffoldKey: _scaffoldKey),
                  Flexible(
                    child: ScreenTypeLayout(
                      mobile: DetailsContentMobile(vehicleInfo: vehicleInfo,),
                      desktop: DetailsContentDesktop(vehicleInfo: vehicleInfo),
                    ),
                  )
                ],
              ),
            ),
          ),
    );
  }
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      key: _scaffoldKey,
//      backgroundColor: Colors.white,
//      body: Container(
//        child: Column(
//          children: <Widget>[
//            NavigationBar(homeScaffoldKey: _scaffoldKey),
//            Expanded(
//              child: ScreenTypeLayout(
//                mobile: DetailsContentMobile(vehicleInfo: vehicleInfo,),
//                desktop: DetailsContentDesktop(vehicleInfo: vehicleInfo),
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }
  }
