import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/firebase.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:yamwheels/models/VehicleInfo.dart';

import '../../Consts.dart';
import '../../widgets/EnsureVisibleWhenFocused.dart';

import 'package:firebase/firestore.dart' as fs;

class DetailsContentDesktop extends StatefulWidget {
  VehicleInfo vehicleInfo;

  DetailsContentDesktop({Key key, @required this.vehicleInfo})
      : super(key: key);

  @override
  _DetailsContentDesktopState createState() => _DetailsContentDesktopState();
}

class _DetailsContentDesktopState extends State<DetailsContentDesktop> {
  FocusNode _focusNodeQuery = new FocusNode();
  final TextEditingController _queryController = new TextEditingController();

  FocusNode _focusNodeContactNo = new FocusNode();
  final TextEditingController _contactNoController =
      new TextEditingController();

  fs.Firestore store;
  fs.CollectionReference ref;

  double rating;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    store = firestore();
    ref = store.collection(Consts.DB_PATH_QUERIES);

    setState(() {
      rating = widget.vehicleInfo.rating;
    });
  }

  addQueryRecord() {
    if (_contactNoController.text.isNotEmpty &&
        _queryController.text.isNotEmpty) {
      Map<String, dynamic> data = {
        Consts.KEY_CONTACT_NO: _contactNoController.text,
        Consts.KEY_QUERY: _queryController.text,
      };
      ref.add(data);

      setState(() {
        _contactNoController.text = '';
        _queryController.text = '';
        _showDialog('Success', 'Your query has been received, we will get back to you soon on your provided contact number.');
      });
    } else {
      _showDialog('Error', 'Please fill the required fileds!');

    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double statusBarHeight = MediaQuery.of(context).padding.top;

    final backgroundContainer = Center(
        child: Container(
      width: size.width,
      height: size.height,
      color: Color(Consts.colorBGLightGrey),
    ));

    final contactNoTextField = Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 48.0,
          width: (size.width / 4),
          child: EnsureVisibleWhenFocused(
            focusNode: _focusNodeContactNo,
            child: Padding(
              padding: EdgeInsets.only(
                  left: 16.0, right: 16.0, top: 0.0, bottom: 0.0),
              child: Container(
                child: TextFormField(
                  style: TextStyle(
                    fontFamily: Consts.PRODUCT_SANS,
                    fontWeight: FontWeight.w400,
                    fontSize: 16.0,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(0.0),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.transparent,
                      ),
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    hintText: '03XX XXXXXXX',
                    hintStyle: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0,
                      color: Colors.blueGrey[300],
                    ),
                  ),
                  keyboardType: TextInputType.phone,
                  maxLines: 1,
                  controller: _contactNoController,
                  focusNode: _focusNodeContactNo,
                ),
              ),
            ),
          ),
        ),
      ],
    );

    final queryTextField = Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 120.0,
          width: (size.width / 4),
          child: EnsureVisibleWhenFocused(
            focusNode: _focusNodeQuery,
            child: Padding(
              padding: EdgeInsets.only(
                  left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
              child: Container(
                child: TextFormField(
                  style: TextStyle(
                    fontFamily: Consts.PRODUCT_SANS,
                    fontWeight: FontWeight.w400,
                    fontSize: 16.0,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(0.0),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.transparent,
                      ),
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    hintText: 'Write your query here...',
                    hintStyle: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0,
                      color: Colors.blueGrey[300],
                    ),
                  ),
                  keyboardType: TextInputType.text,
                  maxLines: 6,
                  controller: _queryController,
                  focusNode: _focusNodeQuery,
                ),
              ),
            ),
          ),
        ),
      ],
    );

    final contactNoContainer = Container(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.all(0.0),
        child: Container(
          height: 48.0,
          decoration: BoxDecoration(
            color: Color(Consts.colorLightGrey),
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: contactNoTextField,
        ),
      ),
    );

    final queryTextContainer = Container(
      alignment: Alignment.topRight,
      child: Padding(
        padding: EdgeInsets.all(0.0),
        child: Container(
          height: 120.0,
          decoration: BoxDecoration(
            color: Color(Consts.colorLightGrey),
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: queryTextField,
        ),
      ),
    );

    final bodyContent = SingleChildScrollView(
      child: Container(
        alignment: Alignment.topCenter,
        margin: EdgeInsets.only(top: statusBarHeight),
        child: Padding(
          padding: EdgeInsets.all(60.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 20.0,
              ),
              Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(6.0),
                      child: Image.asset(
                        Consts.IMAGES_DIRECTORY + widget.vehicleInfo.images[0],
                        height: 450.0,
                        width: (size.width - size.width / 2.5),
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 40.0,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
// Vehicle Name:
                        Text(
                          widget.vehicleInfo.name,
                          style: TextStyle(
                              fontFamily: 'Product Sans',
                              fontWeight: FontWeight.w800,
                              fontSize: 20.0,
                              color: Color(Consts.colorText)),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(
                          height: 4.0,
                        ),
// Vehicle Year & Color:
                        Text(
                          (widget.vehicleInfo.year +
                              ' | ' +
                              widget.vehicleInfo.color),
                          style: TextStyle(
                              fontFamily: 'Product Sans',
                              fontWeight: FontWeight.w400,
                              color: Color(Consts.colorGrey),
                              fontSize: 18.0),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
// Fare:
                        Text(
                          ('Starts from:'),
                          style: TextStyle(
                              fontFamily: 'Product Sans',
                              fontWeight: FontWeight.w400,
                              color: Color(Consts.colorGrey),
                              fontSize: 16.0),
                        ),
                        SizedBox(
                          height: 2.0,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              (widget.vehicleInfo.priceUnit +
                                  ' ' +
                                  widget.vehicleInfo.fare),
                              style: TextStyle(
                                  fontFamily: 'Product Sans',
                                  fontWeight: FontWeight.w800,
                                  color: Color(Consts.colorPrimary),
                                  fontSize: 30.0),
                            ),
                            SizedBox(
                              width: 16.0,
                            ),
                            Text(
                              ('(In ' +
                                  widget.vehicleInfo.city +
                                  ' for ' +
                                  widget.vehicleInfo.availability +
                                  ')'),
                              style: TextStyle(
                                  fontFamily: 'Product Sans',
                                  fontWeight: FontWeight.w400,
                                  color: Color(Consts.colorText),
                                  fontSize: 16.0),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              ('Driver:'),
                              style: TextStyle(
                                  fontFamily: 'Product Sans',
                                  fontWeight: FontWeight.w400,
                                  color: Color(Consts.colorGrey),
                                  fontSize: 16.0),
                            ),
                            SizedBox(
                              height: 2.0,
                            ),
                            Text(
                              (widget.vehicleInfo.driver.name),
                              style: TextStyle(
                                  fontFamily: 'Product Sans',
                                  fontWeight: FontWeight.w800,
                                  color: Color(Consts.colorText),
                                  fontSize: 20.0),
                            ),
                            SizedBox(
                              height: 4.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                FlutterRatingBarIndicator(
                                  rating: rating,
                                  itemCount: 5,
                                  itemSize: 20.0,
                                  emptyColor: Colors.amber.withAlpha(90),
                                ),
                                SizedBox(
                                  width: 4.0,
                                ),
                                Text(
                                  ('(' +
                                      widget.vehicleInfo.rating.toString() +
                                      ')'),
                                  style: TextStyle(
                                      fontFamily: 'Product Sans',
                                      fontWeight: FontWeight.w400,
                                      color: Color(Consts.colorText),
                                      fontSize: 16.0),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 16.0,
                            ),
                            contactNoContainer,
                            SizedBox(
                              height: 8.0,
                            ),
                            queryTextContainer,
                            SizedBox(
                              height: 8.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  onPressed: () => {addQueryRecord()},
                                  color: Color(Consts.colorPrimary),
                                  child: Text(
                                    ('Send Query'),
                                    style: TextStyle(
                                        fontFamily: 'Product Sans',
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white,
                                        fontSize: 16.0),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ]),
              SizedBox(
                height: 20.0,
              ),
              Row(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
                Container(
                  width: (size.width - size.width / 2.5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          (widget.vehicleInfo.amenities.isAc)
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Image.asset(
                                      Consts.AC,
                                      width: 16.0,
                                      height: 16.0,
                                    ),
                                    SizedBox(
                                      width: 8.0,
                                    ),
                                    Text(
                                      ('AC'),
                                      style: TextStyle(
                                          fontFamily: 'Product Sans',
                                          fontWeight: FontWeight.w400,
                                          color: Color(Consts.colorText),
                                          fontSize: 16.0),
                                    ),
                                  ],
                                )
                              : Text(
                                  '',
                                  style: TextStyle(
                                      fontFamily: 'Product Sans',
                                      fontWeight: FontWeight.w400,
                                      color: Color(Consts.colorText),
                                      fontSize: 16.0),
                                ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Image.asset(
                                Consts.USER,
                                width: 16.0,
                                height: 16.0,
                              ),
                              SizedBox(
                                width: 8.0,
                              ),
                              Text(
                                ('${widget.vehicleInfo.amenities.capacity} Persons'),
                                style: TextStyle(
                                    fontFamily: 'Product Sans',
                                    fontWeight: FontWeight.w400,
                                    color: Color(Consts.colorText),
                                    fontSize: 16.0),
                              ),
                            ],
                          ),
                          (widget.vehicleInfo.amenities.isMultimedia)
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Image.asset(
                                      Consts.MULTIMEDIA,
                                      width: 16.0,
                                      height: 16.0,
                                    ),
                                    SizedBox(
                                      width: 8.0,
                                    ),
                                    Text(
                                      ('Multimedia'),
                                      style: TextStyle(
                                          fontFamily: 'Product Sans',
                                          fontWeight: FontWeight.w400,
                                          color: Color(Consts.colorText),
                                          fontSize: 16.0),
                                    ),
                                  ],
                                )
                              : Text(
                                  '',
                                  style: TextStyle(
                                      fontFamily: 'Product Sans',
                                      fontWeight: FontWeight.w400,
                                      color: Color(Consts.colorText),
                                      fontSize: 16.0),
                                ),
                        ],
                      ),
                      SizedBox(
                        height: 40.0,
                      ),
                      Text(
                        ('Details:'),
                        style: TextStyle(
                            fontFamily: 'Product Sans',
                            fontWeight: FontWeight.w400,
                            color: Color(Consts.colorGrey),
                            fontSize: 16.0),
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 20.0),
                        child: Text(
                          (widget.vehicleInfo.details),
                          style: TextStyle(
                              fontFamily: 'Product Sans',
                              fontWeight: FontWeight.w400,
                              color: Color(Consts.colorText),
                              fontSize: 16.0),
                        ),
                      ),
                    ],
                  ),
                ),
//                SizedBox(
//                  width: 40.0,
//                ),
//                Column(
//                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: <Widget>[
//                    Text(
//                      ('Driver:'),
//                      style: TextStyle(
//                          fontFamily: 'Product Sans',
//                          fontWeight: FontWeight.w400,
//                          color: Color(Consts.colorGrey),
//                          fontSize: 16.0),
//                    ),
//                    SizedBox(
//                      height: 2.0,
//                    ),
//                    Text(
//                      (widget.vehicleInfo.driver.name),
//                      style: TextStyle(
//                          fontFamily: 'Product Sans',
//                          fontWeight: FontWeight.w800,
//                          color: Color(Consts.colorText),
//                          fontSize: 20.0),
//                    ),
//                    SizedBox(
//                      height: 4.0,
//                    ),
//                    Row(
//                      mainAxisAlignment: MainAxisAlignment.start,
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      children: <Widget>[
//                        Image.asset(
//                          Consts.IC_STAR,
//                          width: 16.0,
//                          height: 16.0,
//                        ),
//                        Image.asset(
//                          Consts.IC_STAR,
//                          width: 16.0,
//                          height: 16.0,
//                        ),
//                        Image.asset(
//                          Consts.IC_STAR,
//                          width: 16.0,
//                          height: 16.0,
//                        ),
//                        Image.asset(
//                          Consts.IC_STAR,
//                          width: 16.0,
//                          height: 16.0,
//                        ),
//                        Image.asset(
//                          Consts.IC_STAR,
//                          width: 16.0,
//                          height: 16.0,
//                        ),
//                        SizedBox(
//                          width: 4.0,
//                        ),
//                        Text(
//                          ('(' + widget.vehicleInfo.rating.toString() + ')'),
//                          style: TextStyle(
//                              fontFamily: 'Product Sans',
//                              fontWeight: FontWeight.w400,
//                              color: Color(Consts.colorText),
//                              fontSize: 16.0),
//                        ),
//                      ],
//                    ),
//                    SizedBox(
//                      height: 16.0,
//                    ),
//                    contactNoContainer,
//                    SizedBox(
//                      height: 4.0,
//                    ),
//                    queryTextContainer,
//                    SizedBox(
//                      height: 8.0,
//                    ),
//                    Row(
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      mainAxisSize: MainAxisSize.max,
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      children: <Widget>[
//                        FlatButton(
//                          onPressed: () => {addQueryRecord()},
//                          color: Color(Consts.colorPrimary),
//                          child: Text(
//                            ('Send Query'),
//                            style: TextStyle(
//                                fontFamily: 'Product Sans',
//                                fontWeight: FontWeight.w400,
//                                color: Colors.white,
//                                fontSize: 16.0),
//                          ),
//                        )
//                      ],
//                    ),
//                  ],
//                ),
              ]),
            ],
          ),
        ),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          backgroundContainer,
          bodyContent,
        ],
      ),
    );
  }

  void _showDialog(String title, String text) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(
            title,
            style: TextStyle(
                fontFamily: 'Product Sans',
                fontWeight: FontWeight.w800,
                color: Color(Consts.colorText),
                fontSize: 16.0),
          ),
          content: new Text(text,
            style: TextStyle(
                fontFamily: 'Product Sans',
                fontWeight: FontWeight.w400,
                color: Color(Consts.colorText),
                fontSize: 18.0),),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok",
                style: TextStyle(
                    fontFamily: 'Product Sans',
                    fontWeight: FontWeight.w800,
                    color: Color(Consts.colorPrimary),
                    fontSize: 18.0),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
