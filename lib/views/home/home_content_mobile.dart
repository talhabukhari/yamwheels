import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:firebase/firebase.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:yamwheels/models/Driver.dart';
import 'package:yamwheels/models/VehicleAmenities.dart';
import 'package:yamwheels/models/VehicleInfo.dart';
import 'package:yamwheels/views/details/details_view.dart';

import '../../Consts.dart';
import '../../widgets/EnsureVisibleWhenFocused.dart';

import 'package:firebase/firestore.dart' as fs;

class HomeContentMobile extends StatefulWidget {
  @override
  _HomeContentMobileState createState() => _HomeContentMobileState();
}

class _HomeContentMobileState extends State<HomeContentMobile> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  FocusNode _focusNodeSearch = new FocusNode();
  final TextEditingController _searchController = new TextEditingController();

  fs.Firestore store;
  fs.CollectionReference ref;

  Map<String, dynamic> data;
  var _vehiclesDataList = <VehicleInfo>[];
  var _searchDataList = <VehicleInfo>[];
  var _premiumDataList = <VehicleInfo>[];
  var _standardDataList = <VehicleInfo>[];
  var _classicDataList = <VehicleInfo>[];

  bool isLoading = true;
  bool isSearch = false;

  doSearch(String query) async {}

  fetchRecords() {
    isLoading = true;
    ref.get().then((results) {
      setState(() {
        if (results != null) {
          _vehiclesDataList.clear();
          _premiumDataList.clear();
          _standardDataList.clear();
          _classicDataList.clear();

          for (int i = 0; i < results.docs.length; i++) {
            VehicleInfo vehicleInfo = new VehicleInfo();
            vehicleInfo.id = results.docs[i].id;
            vehicleInfo.availability =
                results.docs[i].data()[Consts.KEY_AVAILABILITY];
            vehicleInfo.city = results.docs[i].data()[Consts.KEY_CITY];
            vehicleInfo.color = results.docs[i].data()[Consts.KEY_COLOR];
            vehicleInfo.details = results.docs[i].data()[Consts.KEY_DETAILS];
            vehicleInfo.fare = results.docs[i].data()[Consts.KEY_FARE];
            vehicleInfo.images =
                List.from(results.docs[i].data()[Consts.KEY_IMAGES]);
            vehicleInfo.name = results.docs[i].data()[Consts.KEY_NAME];
            vehicleInfo.priceUnit =
                results.docs[i].data()[Consts.KEY_PRICE_UNIT];
            vehicleInfo.rating = results.docs[i].data()[Consts.KEY_RATING];
            vehicleInfo.type = results.docs[i].data()[Consts.KEY_TYPE];
            vehicleInfo.year = results.docs[i].data()[Consts.KEY_YEAR];

            Map<String, dynamic> va =
                results.docs[i].data()[Consts.KEY_AMENITIES];
            VehicleAmenities vehicleAmenities = new VehicleAmenities();
            vehicleAmenities.capacity = va[Consts.KEY_CAPACITY];
            vehicleAmenities.isAc = va[Consts.KEY_IS_AC];
            vehicleAmenities.isMultimedia = va[Consts.KEY_IS_MULTIMEDIA];

            vehicleInfo.amenities = vehicleAmenities;

            fs.DocumentReference driverDocumentRef =
                results.docs[i].data()[Consts.KEY_DRIVER];
            driverDocumentRef.get().then((value) {
              Driver driver = new Driver();
              driver.name = value.get('name');
              driver.isMale = value.get('isMale');

              vehicleInfo.driver = driver;

              _vehiclesDataList.add(vehicleInfo);

              if (vehicleInfo.type == Consts.TYPE_PREMIUM)
                _premiumDataList.add(vehicleInfo);

              if (vehicleInfo.type == Consts.TYPE_STANDARD)
                _standardDataList.add(vehicleInfo);

              if (vehicleInfo.type == Consts.TYPE_CLASSIC)
                _classicDataList.add(vehicleInfo);
            });
          }
          _searchDataList = _vehiclesDataList;
          isLoading = false;
        }
      });
    });
  }

  void onSearchTextChange() {
    setState(() {
      if (_searchController.text.length > 0) {
        _searchDataList = _vehiclesDataList;
        var _tempDataList = <VehicleInfo>[];
        isSearch = true;
        for (int i = 0; i < _vehiclesDataList.length; i++) {
          if (_vehiclesDataList[i]
                  .name
                  .toString()
                  .toLowerCase()
                  .contains(_searchController.text.toLowerCase()) ||
              _vehiclesDataList[i]
                  .city
                  .toString()
                  .toLowerCase()
                  .contains(_searchController.text.toLowerCase()) ||
              _vehiclesDataList[i]
                  .driver
                  .name
                  .toString()
                  .toLowerCase()
                  .contains(_searchController.text.toLowerCase()) ||
              _vehiclesDataList[i]
                  .fare
                  .toString()
                  .toLowerCase()
                  .contains(_searchController.text.toLowerCase()) ||
              _vehiclesDataList[i]
                  .type
                  .toString()
                  .toLowerCase()
                  .contains(_searchController.text.toLowerCase()) ||
              _vehiclesDataList[i]
                  .color
                  .toString()
                  .toLowerCase()
                  .contains(_searchController.text.toLowerCase()) ||
              _vehiclesDataList[i]
                  .availability
                  .toString()
                  .toLowerCase()
                  .contains(_searchController.text.toLowerCase()) ||
              _vehiclesDataList[i]
                  .details
                  .toString()
                  .toLowerCase()
                  .contains(_searchController.text.toLowerCase()) ||
              _vehiclesDataList[i]
                  .rating
                  .toString()
                  .toLowerCase()
                  .contains(_searchController.text.toLowerCase()))
            _tempDataList.add(_vehiclesDataList[i]);
        }
        _searchDataList = _tempDataList;
      } else {
        isSearch = false;
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    store = fb.firestore();
    ref = store.collection(Consts.DB_PATH_VEHICLES);

    _searchController.addListener(onSearchTextChange);

    setState(() {
      _vehiclesDataList.clear();
      _premiumDataList.clear();
      _standardDataList.clear();
      _classicDataList.clear();
      isLoading = true;
    });

    ref.onSnapshot.listen((querySnapshot) {
      querySnapshot.docChanges().forEach((change) {
        VehicleInfo vehicleInfo = new VehicleInfo();
        vehicleInfo.id = change.doc.id;
        vehicleInfo.availability = change.doc.data()[Consts.KEY_AVAILABILITY];
        vehicleInfo.city = change.doc.data()[Consts.KEY_CITY];
        vehicleInfo.color = change.doc.data()[Consts.KEY_COLOR];
        vehicleInfo.details = change.doc.data()[Consts.KEY_DETAILS];
        vehicleInfo.fare = change.doc.data()[Consts.KEY_FARE];
        vehicleInfo.images = List.from(change.doc.data()[Consts.KEY_IMAGES]);
        vehicleInfo.name = change.doc.data()[Consts.KEY_NAME];
        vehicleInfo.priceUnit = change.doc.data()[Consts.KEY_PRICE_UNIT];
        vehicleInfo.rating = change.doc.data()[Consts.KEY_RATING];
        vehicleInfo.type = change.doc.data()[Consts.KEY_TYPE];
        vehicleInfo.year = change.doc.data()[Consts.KEY_YEAR];

        Map<String, dynamic> va = change.doc.data()[Consts.KEY_AMENITIES];
        VehicleAmenities vehicleAmenities = new VehicleAmenities();
        vehicleAmenities.capacity = va[Consts.KEY_CAPACITY];
        vehicleAmenities.isAc = va[Consts.KEY_IS_AC];
        vehicleAmenities.isMultimedia = va[Consts.KEY_IS_MULTIMEDIA];
        vehicleInfo.amenities = vehicleAmenities;

        fs.DocumentReference driverDocumentRef =
            change.doc.data()[Consts.KEY_DRIVER];
        driverDocumentRef.get().then((value) {
          Driver driver = new Driver();
          driver.name = value.get('name');
          driver.isMale = value.get('isMale');
          vehicleInfo.driver = driver;

          if (change.type == Consts.FB_CHANGE_ADDED) {
            setState(() {
              _vehiclesDataList.add(vehicleInfo);
              if (vehicleInfo.type == Consts.TYPE_PREMIUM)
                _premiumDataList.add(vehicleInfo);

              if (vehicleInfo.type == Consts.TYPE_STANDARD)
                _standardDataList.add(vehicleInfo);

              if (vehicleInfo.type == Consts.TYPE_CLASSIC)
                _classicDataList.add(vehicleInfo);
            });
          } else if (change.type == Consts.FB_CHANGE_MODIFIED ||
              change.type == Consts.FB_CHANGE_REMOVED) {
            fetchRecords();
          }
        });
        isLoading = false;
        _searchDataList = _vehiclesDataList;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double statusBarHeight = MediaQuery.of(context).padding.top;

    /*24 is for notification bar on Android*/
//    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemHeight = 80;
    final double itemWidth = size.width / 2;

    final backgroundContainer = Center(
        child: Container(
      width: size.width,
      height: size.height,
      color: Color(Consts.colorBGLightGrey),
    ));

    final searchEditTextField = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 48.0,
          child: EnsureVisibleWhenFocused(
            focusNode: _focusNodeSearch,
            child: Padding(
              padding: EdgeInsets.only(left: 16.0, right: 4.0),
              child: Container(
                child: TextFormField(
                  style: TextStyle(
                    fontFamily: Consts.PRODUCT_SANS,
                    fontWeight: FontWeight.w400,
                    fontSize: 16.0,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(0.0),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.transparent,
                      ),
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    hintText: 'Search...',
                    hintStyle: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0,
                      color: Colors.blueGrey[300],
                    ),
                  ),
                  keyboardType: TextInputType.text,
                  maxLines: 1,
                  controller: _searchController,
                  focusNode: _focusNodeSearch,
                ),
              ),
            ),
          ),
        ),
      ],
    );
    final searchBarContainer = Container(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Container(
          height: 48.0,
          decoration: BoxDecoration(
            color: Color(Consts.colorLightGrey),
            borderRadius: BorderRadius.all(
              Radius.circular(24.0),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: searchEditTextField,
              ),
              GestureDetector(
                onTap: () => {
                  FocusScope.of(context).requestFocus(new FocusNode()),
                  doSearch(_searchController.text)
                },
                child: Padding(
                    padding: EdgeInsets.all(12.0),
                    child: Image.asset(
                      Consts.IC_SEARCH,
                      height: 24,
                      width: 24.0,
                      color: Color(Consts.colorPrimary),
                    )),
              ),
            ],
          ),
        ),
      ),
    );

    final topContainer = Stack(
      children: <Widget>[
        searchBarContainer,
      ],
    );

    final premiumTopView = Container(
      margin: EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 8.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Padding(
                  padding: EdgeInsets.only(left: 16.0),
                  child: Text(
                    'Premium',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 18.0,
                      color: Color(Consts.colorText),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => {},
                child: Padding(
                  padding: EdgeInsets.only(right: 24.0),
                  child: Text(
                    'See All',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 0.0,
          ),
        ],
      ),
    );
    final premiumListViewContainer = Flexible(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, right: 0.0, left: 0.0),
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            key: _formKey,
            child: GridView.builder(
              shrinkWrap: true,
              itemCount: _premiumDataList.length,
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 8),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                childAspectRatio: (itemWidth / itemHeight),
              ),
              itemBuilder: (context, index) {
                return _buildCard(_premiumDataList[index]);
              },
            ),
          ),
        ),
      ),
    );

    final standardTopView = Container(
      margin: EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 8.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Padding(
                  padding: EdgeInsets.only(left: 16.0),
                  child: Text(
                    'Standard',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 18.0,
                      color: Color(Consts.colorText),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => {},
                child: Padding(
                  padding: EdgeInsets.only(right: 24.0),
                  child: Text(
                    'See All',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 0.0,
          ),
        ],
      ),
    );
    final standardListViewContainer = Flexible(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, right: 0.0, left: 0.0),
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            child: ListView.builder(
              itemCount: _standardDataList.length,
              itemBuilder: (context, position) {
                return _buildCard(_standardDataList[position]);
              },
            ),
          ),
        ),
      ),
    );

    final classicTopView = Container(
      margin: EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 8.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Padding(
                  padding: EdgeInsets.only(left: 16.0),
                  child: Text(
                    'Classic',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 18.0,
                      color: Color(Consts.colorText),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => {},
                child: Padding(
                  padding: EdgeInsets.only(right: 24.0),
                  child: Text(
                    'See All',
                    style: TextStyle(
                      fontFamily: Consts.PRODUCT_SANS,
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 0.0,
          ),
        ],
      ),
    );
    final classicListViewContainer = Flexible(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, right: 0.0, left: 0.0),
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            child: ListView.builder(
              itemCount: _classicDataList.length,
              itemBuilder: (context, position) {
                return _buildCard(_classicDataList[position]);
              },
            ),
          ),
        ),
      ),
    );

    final searchListViewContainer = Flexible(
      child: Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, right: 0.0, left: 0.0),
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            child: ListView.builder(
              itemCount: _searchDataList.length,
              itemBuilder: (context, position) {
                return _buildCard(_searchDataList[position]);
              },
            ),
          ),
        ),
      ),
    );

    final searchGridViewContainer = Container(
      child: GridView.builder(
        shrinkWrap: true,
        itemCount: _searchDataList.length,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.symmetric(horizontal: 8),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          childAspectRatio: (itemWidth / itemHeight),
        ),
        itemBuilder: (context, index) {
          return _buildCard(_searchDataList[index]);
        },
      ),
    );

    final bodyContent = SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Container(
          alignment: Alignment.topLeft,
//          margin: EdgeInsets.only(top: statusBarHeight),
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: Column(
              children: <Widget>[
                searchBarContainer,
                (isSearch)
                    ? searchGridViewContainer
                    : Column(
                        children: <Widget>[
                          premiumTopView,
                          (_premiumDataList != null &&
                                  _premiumDataList.length > 0)
                              ? _buildCard(_premiumDataList[0])
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                          (_premiumDataList != null &&
                                  _premiumDataList.length > 1)
                              ? _buildCard(_premiumDataList[1])
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                          (_premiumDataList != null &&
                                  _premiumDataList.length > 2)
                              ? _buildCard(_premiumDataList[2])
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                          standardTopView,
                          (_standardDataList != null &&
                                  _standardDataList.length > 0)
                              ? _buildCard(_standardDataList[0])
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                          (_standardDataList != null &&
                                  _standardDataList.length > 1)
                              ? _buildCard(_standardDataList[1])
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                          (_standardDataList != null &&
                                  _standardDataList.length > 2)
                              ? _buildCard(_standardDataList[2])
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                          classicTopView,
                          (_classicDataList != null &&
                                  _classicDataList.length > 0)
                              ? _buildCard(_classicDataList[0])
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                          (_classicDataList != null &&
                                  _classicDataList.length > 1)
                              ? _buildCard(_classicDataList[1])
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                          (_classicDataList != null &&
                                  _classicDataList.length > 2)
                              ? _buildCard(_classicDataList[2])
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                        ],
                      ),
              ],
            ),
          ),
        ),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          backgroundContainer,
          bodyContent,
        ],
      ),
    );
  }

//  Future<String> _getImageUrl(String filename) async {
//    final Future<StorageReference> ref = FirebaseStorage.instance
//        .getReferenceFromUrl(Consts.DB_PATH_STORAGE + filename);
//    dynamic url = await ref.then((doc) => doc.getDownloadURL());
//    print('--------img url---' + url);
//    return url;
//  }

  Future<dynamic> loadImage(BuildContext context, String image) async {
//    var url = await storage().ref(image).getDownloadURL();
    print('-----------imiiii--here-url00-----------' + image);
    var url2 =
        await FirebaseStorage.instance.ref().child(image).getDownloadURL();
    print('-----------imiiii--here-url-----------' + url2.toString());
    return url2;
  }

  Widget _buildCard(VehicleInfo object) {
    String imgUrl = object.images[0];
//    String imgUrl = '';
//    if (object.images != null && object.images.length > 0) {
//      loadImage(context, Consts.DB_PATH_STORAGE + object.images[0]).then((url) {
//        setState(() {
//          imgUrl = url.toString();
//        });
//      });
//    }
    String name = object.name;
    String modelYear = object.year;
    String color = object.color;
    String availability = object.availability;
    String price = '${object.priceUnit} ${object.fare}';
    var rating = object.rating;

    final row = GestureDetector(
      onTap: () => {_listItemTapped(object)},
      child: Container(
        child: Card(
          semanticContainer: true,
          elevation: 1.0,
          color: Colors.white,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(8.0)),
          margin:
              EdgeInsets.only(top: 4.0, bottom: 8.0, right: 16.0, left: 16.0),
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Container(
              margin:
                  EdgeInsets.only(top: 0.0, right: 0.0, left: 0.0, bottom: 0.0),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(6.0),
                            child:
//                            (imgUrl != null && imgUrl.isNotEmpty)
//                                ? CachedNetworkImage(
//                                    width: 110.0,
//                                    height: 110.0,
//                                    imageUrl: imgUrl,
//                                    fit: BoxFit.cover,
//                                    placeholder: (context, url) => Image.asset(
//                                      Consts.PLACEHOLDER_LOADING_CAR,
//                                    ),
//                                  )
//                                :
                                Image.asset(
                              (Consts.IMAGES_DIRECTORY + imgUrl),
                              width: 110.0,
                              height: 110.0,
                              fit: BoxFit.cover,
                            ),
                          ),
                          SizedBox(
                            width: 16.0,
                          ),
                          Flexible(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            name,
                                            style: TextStyle(
                                                fontFamily: 'Product Sans',
                                                fontWeight: FontWeight.w400,
                                                fontSize: 16.0,
                                                color: Colors.black),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: 4.0,
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          rating.toString(),
                                          style: TextStyle(
                                              fontFamily: 'Product Sans',
                                              fontWeight: FontWeight.w400,
                                              color: Color(Consts.colorText),
                                              fontSize: 14.0),
                                        ),
                                        SizedBox(
                                          width: 4.0,
                                        ),
                                        Image.asset(Consts.IC_STAR,
                                            width: 14.0, height: 14.0),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      modelYear,
                                      style: TextStyle(
                                          fontFamily: 'Product Sans',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16.0,
                                          color: Color(Consts.colorGrey)),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Text(
                                      ' | ',
                                      style: TextStyle(
                                          fontFamily: 'Product Sans',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16.0,
                                          color: Color(Consts.colorGrey)),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Text(
                                      color,
                                      style: TextStyle(
                                          fontFamily: 'Product Sans',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16.0,
                                          color: Color(Consts.colorGrey)),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 16.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Flexible(
                                      child: Text(
                                        '',
                                        style: TextStyle(
                                            fontFamily: 'Product Sans',
                                            fontWeight: FontWeight.w400,
                                            fontSize: 18.0,
                                            color: Colors.black),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    Text(
                                      'Starts at',
                                      style: TextStyle(
                                          fontFamily: 'Product Sans',
                                          fontWeight: FontWeight.w400,
                                          color: Color(Consts.colorGrey),
                                          fontSize: 14.0),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            availability,
                                            style: TextStyle(
                                                fontFamily: 'Product Sans',
                                                fontWeight: FontWeight.w400,
                                                fontSize: 16.0,
                                                color: Color(Consts.colorGrey)),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          price,
                                          style: TextStyle(
                                              fontFamily: 'Product Sans',
                                              fontWeight: FontWeight.w800,
                                              color: Color(Consts.colorPrimary),
                                              fontSize: 16.0),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ]),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
    return row;
  }

  Future _listItemTapped(VehicleInfo object) async {
    Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (c, a1, a2) => DetailsView(
          vehicleInfo: object,
        ),
        transitionsBuilder: (c, anim, a2, child) =>
            FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 300),
      ),
    );
  }
}
