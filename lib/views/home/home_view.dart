
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:yamwheels/widgets/centered_view/centered_view.dart';
import 'package:yamwheels/widgets/nav_drawer/navigation_drawer.dart';
import 'package:yamwheels/widgets/navigation_bar/navigation_bar.dart';

import 'home_content_desktop.dart';
import 'home_content_mobile.dart';

class HomeView extends StatelessWidget {

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) => Scaffold(
        key: _scaffoldKey,
        drawer: sizingInformation.deviceScreenType == DeviceScreenType.Mobile
            ? NavigationDrawer()
            : null,
        backgroundColor: Colors.white,
//        body: CenteredView(
        body: Container(
          child: Column(
            children: <Widget>[
              NavigationBar(homeScaffoldKey: _scaffoldKey),
              Flexible(
                child: ScreenTypeLayout(
                  mobile: HomeContentMobile(),
                  desktop: HomeContentDesktop(),
                ),
              )
            ],
          ),
        ),
      ),
    );

//      Scaffold(
//      backgroundColor: Colors.white,
//      body: CenteredView(
//        child: Column(
//          children: <Widget>[
//            NavigationBar(),
//            Expanded(
//              child: ScreenTypeLayout(
//                mobile: HomeContentMobile(),
//                desktop: HomeContentDesktop(),
//              ),
//            )
//          ],
//        ),
//      ),
//    );
  }
}
