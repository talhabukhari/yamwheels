import 'package:yamwheels/models/Driver.dart';

import 'VehicleAmenities.dart';

class VehicleInfo {
  String id;
  String availability;
  String city;
  String color;
  String details;
  Driver driver;
  String fare;
  List<String> images;
  String name;
  String priceUnit;
  var rating;
  String type;
  VehicleAmenities amenities;
  String year;
}